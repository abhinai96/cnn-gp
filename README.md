Project on Image classification without using any CNN external libraries.
I have downloaded the image dataset of dogs vs cats from kaggle.I have uploaded those images in google drive in order run in google colab.
Those image dataset are used the training the model,but i have not uploaded the dataset in gitlab account.
I have used libraries of computer vision(cv) to read the images.
Then converting the color image to grey image.
Download the haarcascade files from the internet inorder to detect the eyes and face of the image.
I have used matplotlib library for visualization of the image.
Then in some of the images ,we find the images with the other backgrounds, namely humans catching dogs in the picture.so inorder to remove those human images we crop the image so that cat or dog face is only visible.
Now we read the entire dataset from the respective folders,in the code i have mentioned the folders as 'project/sample_data/data',that means i had project direcory and in it i had sampe_data directory with the data directory in it which has more images.
Used os module to read the data from the system
I have used wavelet transformed image, you can see edges clearly and that can give us clues on various facial features such as eyes, nose, lips etc
After data cleaning process is done,then train the model.We will use SVM with rbf kernel tuned with heuristic finetuning.
Compared the model accuracy with the algoritms and then seen the svm model is given the best accuracy,so that used the svm model for hyperparameter tuning.
Then save the model to pickle file.


